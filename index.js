"use strict"




class Employee {
    constructor(name,age,salary) {
        this._name =  name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
      return this._name
      }
    get age() {
        return this._age
        }
    get salary() {
        return this._salary
    }
    set name(value) {
        this._name = value
    }
    set age(value) {
        this._age = value
    }
    set salary(value) {
        this._salary = value
    }
}

class Programmer extends Employee {

    constructor(name,age,salary,lang) {
        super(name,age,salary);
        this._lang = lang

    }
    get salary() {

        return this._salary*3
    }


}

    const employee = new Employee('Dracula',300,1000);
    const programmer = new Programmer('Petro',30,1000,'JS');
    const programmer2 = new Programmer('Petros',33,2000, 'Python');
    const programmer3 = new Programmer('Petrik',36,3000, 'Basic');

    console.log(employee)
    console.log(programmer);
    console.log(programmer2);
    console.log(programmer3);
    employee.name="Petia"
    employee.age=25
    employee.salary=5000

    console.log(employee.name)
    console.log(employee.age)
    console.log(employee.salary)
    console.log(employee)

    console.log(programmer3.salary);

